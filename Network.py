import numpy as np
from matplotlib import pyplot as plt
from sklearn.utils import shuffle
from FCLayer import FCLayer
from dataGen import dataGen
from config_manager import config_manager

class Network:
    np.set_printoptions(suppress=True)  # Print without scientific notation

    def __init__(self):  # Default parameters
        self.conf = config_manager()
        self.data_gen = dataGen()
        self.data_gen.generate_dataset(*self.conf.fetch_datagen_param())  # Initialize dataset
        (self.n_layers,
         self.ni_neurons,
         self.no_neurons,
         self.loss_func,
         self.weight_reg,
         self.weight_reg_rate,
         self.min_weight,
         self.max_weight,
         self.activation_function,
         self.softmax,
         self.learning_rate,
         self.batch_size,
         self.verbose,

         self.layer_size,
         self.layer_act,
         self.layer_min_weight,
         self.layer_max_weight,
         self.lr) = self.conf.fetch_net_data()  # Neural net config data

        self.layers = []

        for i in range(self.n_layers+2):  # +2 for output and input
            if i == 0:  # Input layer
                new_layer = FCLayer(self.ni_neurons,
                                    self.ni_neurons,
                                    i,
                                    "linear",
                                    self.weight_reg,
                                    self.weight_reg_rate,
                                    self.batch_size,
                                    self.learning_rate)
                self.add(new_layer)
            elif i == (self.n_layers+2) - 1:  # Last layer (output layer)
                if self.n_layers == 0:  # Output layer following input layer
                    new_layer = FCLayer(self.ni_neurons,
                                        self.no_neurons,
                                        i,
                                        self.layer_act[i-1],
                                        self.weight_reg,
                                        self.weight_reg_rate,
                                        self.batch_size,
                                        self.lr[i-1],
                                        self.layer_min_weight[i-1],
                                        self.layer_max_weight[i-1])
                    self.add(new_layer)
                else:   # Output layer
                    new_layer = FCLayer(self.layer_size[i-2],   # Input size of last layer
                                        self.no_neurons,
                                        i,
                                        self.layer_act[i-1],
                                        self.weight_reg,
                                        self.weight_reg_rate,
                                        self.batch_size,
                                        self.lr[i-1],
                                        self.layer_min_weight[i - 1],
                                        self.layer_max_weight[i - 1])
                    self.add(new_layer)
                if self.softmax:    # Softmax layer (if applicable)
                    new_layer = FCLayer(self.no_neurons,
                                        self.no_neurons,
                                        i+1,
                                        "softmax",
                                        self.weight_reg,
                                        self.weight_reg_rate,
                                        self.batch_size)
                    self.add(new_layer)
            else:  # Hidden layers
                if i == 1:  # First layer receives input from input layer
                    new_layer = FCLayer(self.ni_neurons,
                                        self.layer_size[i-1],
                                        i,
                                        self.layer_act[i-1],
                                        self.weight_reg,
                                        self.weight_reg_rate,
                                        self.batch_size,
                                        self.lr[i-1],
                                        self.layer_min_weight[i - 1],
                                        self.layer_max_weight[i - 1])
                    self.add(new_layer)
                else:   # Normal in-between hidden layer
                    new_layer = FCLayer(self.layer_size[i-2],
                                        self.layer_size[i-1],
                                        i,
                                        self.layer_act[i-1],
                                        self.weight_reg,
                                        self.weight_reg_rate,
                                        self.batch_size,
                                        self.lr[i-1],
                                        self.layer_min_weight[i - 1],
                                        self.layer_max_weight[i - 1])
                    self.add(new_layer)

    def add(self, layer):
        self.layers.append(layer)

    def predict(self, inputs):  # Using forward pass to predict
        result = []  # Array for keeping the output layer result
        for i in range(len(inputs)):
            output = inputs[i].flatten()  # First initialized output from the data
            for layer in self.layers:
                output = layer.forward_pass(output)  # output is recursively fed through all the layers
            result.append(output)

        return result

    def fit_single(self, epochs):
        """
        Runs single sample cases through the neural network
        :param epochs:  Number of epochs to be run
        :return:
        """
        self.print_network_structure()
        X_train, y_train, X_test, y_test, X_val, y_val = self.data_gen.fetch_complete_data()
        self.data_gen.show_images(X_train)

        # Initializing parameters / buffers:
        samples = len(X_train)
        result = 0      # Result from forward pass
        error = 0       # Loss function gradient error
        err = 0         # Loss function error
        val_err = 0     # Validation error

        errors = []         # Training errors
        test_errors = []    # Testing errors
        val_errors = []     # Validation errors

        plt.ioff()       # Interactive plotting (doesnt work?)

        for ep in range(epochs):
            X_train, y_train = shuffle(X_train, y_train)    # Shuffle the data every epoch
            for i in range(samples):
                ##########################################
                trainX = X_train[i]        # Pick sample
                trainY = y_train[i]        # Pick target

                valX = X_test[i % len(X_test)]   # Pick testing sample
                valY = y_test[i % len(y_test)]   # Pick testing target

                trainX = trainX.flatten().T       # Turn into a 1D vector for the first layer
                valX = valX.flatten().T           #
                ##########################################

                val_result = self.forward_pass(valX)
                result = self.forward_pass(trainX)  # Initiating forward pass with minibatch

                if self.loss_func == 'cross-entropy':
                    err = self.cross_entropy(trainY, result.flatten())  # Error
                    val_err = (self.cross_entropy(valY, val_result.flatten()))
                    error = self.cross_entropy_prime(trainY, result.flatten())
                elif self.loss_func == 'mse':
                    err = (self.mse(trainY, result.flatten()))
                    val_err = self.mse(valY, val_result.flatten())
                    error = self.mse_prime(trainY, result.flatten())

                if self.verbose:
                    print("-----")
                    print(trainX)
                    print(trainY)
                    print(result.flatten())
                    print(err)

                if i % 1 == 0:          # Adjustment for how often errors are gathered for plotting
                    errors.append(err)
                    val_errors.append(val_err)

                self.backward_pass(result, error)

            # Plotting: Train and test
            plt.pause(0.0001)
            plt.figure()
            plt.plot(self.smooth(errors, 50))
            plt.plot(self.smooth(val_errors, 400))
            plt.title("Performance")
            plt.ylabel("Loss")
            plt.xlabel("Epoch")
            plt.legend(['Train', 'Val'], loc = 'upper left')
            plt.show()

        # Running through test set
        error = 0
        result = self.predict(X_test)
        for i in range(len(X_test)):
            error = self.cross_entropy(y_test[i], result[i].flatten())
            test_errors.append(error)
            print(error)
        print("\nMean testing error: " + str(np.mean(test_errors)))

    def forward_pass(self, sample_x):
        # One run through all the layers
        output = sample_x
        for layer in self.layers:
            output = layer.forward_pass(output)
        return output

    def backward_pass(self, result, initial_error):
        J_ls = initial_error

        # Calculate initial J_lz = J_ls * J_sz (loss w.r.t the softmax layer)
        if self.softmax:
            J_lz = np.dot(J_ls, self.softmax_prime(result))
            skip_last = True    # Skips softmax layer (calculating J_lz here instead)
        else:
            J_lz = initial_error
            skip_last = False

        # Calculate the error, then send that to the last layer to set off the chain
        for layer in reversed(self.layers):
            if skip_last:
                skip_last = False
            else:
                J_lz = layer.backward_pass_tensor(J_lz)

    # loss function and its derivative
    def mse(self, y_true, y_pred):
        return np.mean(np.power(y_true - y_pred, 2))

    def mse_prime(self, y_true, y_pred):
        return 2 * (y_pred - y_true) / y_true.size

    def softmax_prime(self, s):
        s = s.flatten()  # Softmaxed output
        J_zs = np.zeros((self.no_neurons, self.no_neurons))
        for j in range(self.no_neurons):
            for i in range(self.no_neurons):
                if j == i:  # The diagonal of the jacobian
                    J_zs[j][i] = s[j] - s[j]**2
                else:       # Everything else (i != j)
                    J_zs[j][i] = -s[j]*s[i]
        return J_zs

    def cross_entropy(self, y_true, y_pred):
        return -1.0 * (y_true * np.log(y_pred)).mean()

    def cross_entropy_prime(self, y_true, y_pred):
        return y_pred - y_true

    def L1(self):
        """
        SUM(Absolute values of all weights across the neural net)
        """
        sum = 0
        for layer in self.layers:
            sum += np.sum(np.absolute(layer.fetch_weights()))
        return sum

    def L2(self):
        """
        1/2 * SUM(All squared weights of the NN):
        """
        sum = 0
        for layer in self.layers:
            sum += np.sum(np.square(layer.fetch_weights()))
        return 0.5 * sum

    def test(self, X_test, y_test):
        error = 0
        result = self.predict(X_test)
        for i in range(len(X_test)):
            error += self.cross_entropy(y_test[i], result[i].flatten())
        return error / len(X_test)  # Return average cross entropy test score

    def val(self, X_val, y_val):
        error = 0
        result = self.predict(X_val)
        for i in range(len(X_val)):
            error += self.cross_entropy(y_val[i], result[i].flatten())
        return error / len(X_val)   # Return average cross entropy validation score

    def print_network_structure(self):
        count = 0
        for layer in self.layers:
            count += 1
            print("Layer: "+str(count) + " | Nodes: " + str(layer.get_n_nodes()) + " | Act: " + layer.get_act_method())

    def smooth(self, y, box_pts):       # Function for removing noise out of plot (smoothing out)
        box = np.ones(box_pts) / box_pts
        y_smooth = np.convolve(y, box, mode='same')
        return y_smooth