import random
import numpy as np
from layer import Layer
import math


# Fully connected layer:
class FCLayer(Layer):

    def __init__(self, n_inputs, n_neurons, layer_no, activation, regularization, reg_rate, batch_size=1, lr=0.05, min_weight=-0.1, max_weight=0.1):
        self.layer_no = layer_no
        self.n_neurons = n_neurons
        self.lr = lr

        if not (layer_no == 0 or activation == "softmax"):  # Not first layer or softmax (without weights and biases)
            self.weights = np.random.uniform(low=min_weight, high=max_weight, size=(n_inputs, n_neurons))
            self.biases = np.random.rand(n_neurons)  # Randomly initializing weights (learns faster!)

        self.input = 0                  # Input to this layer
        self.output = 0                 # Activated sum
        self.act_method = activation    # Activation method used
        self.delta_w = 0                 # Buffer variable for storing weight gradients
        self.delta_b = 0                 # Buffer variable for storing bias gradients
        self.batch_len = 0              # State of batch progress
        self.batch_size = batch_size    # Batch length before updating happens

        self.regularization = regularization    # Regularization method (L1, L2 or None)
        self.reg_rate = reg_rate                # The regularization rate for adjustment of gradient magnitude


        if activation == "relu":
            self.activation = self.ReLU
            self.activation_prime = self.ReLU_prime
        if activation == "sigmoid":
            self.activation = self.sigmoid
            self.activation_prime = self.sigmoid_prime
        if activation == "tanh":
            self.activation = self.tanh
            self.activation_prime = self.tanh_prime
        if activation == "linear":
            self.activation = self.linear
            self.activation_prime = self.linear_prime
        if activation == "softmax":
            self.activation = self.softmax


    # Transpose weight array when implementing minibatch
    def forward_pass(self, input):
        self.input = input      # Save input from previous layer for backward pass

        if self.act_method != "softmax" and self.layer_no != 0:  # Calculate sum for any other layer than softmax layer
            self.output = np.dot(input, self.weights) + self.biases # Calculating the output
        else:
            self.output = input        # For softmax, just activate the input

        self.output = self.activation(self.output)   # Run the unactivated value through activation function

        return self.output     # Return activated value h

    def backward_pass_tensor(self, J_lz):
        """
        :param J_lz: Output error from downstream layer (Loss prime w.r.t input to next layer
        """
        if self.layer_no != 0:  # Not input layer
            # partial derivative of z w.r.t sum (wx + b)
            # Gradient of activated sum with respect to the summed inputs
            J_zsum = np.diag(self.activation_prime(self.output.flatten()))  # derivative of activation function w.r.t the sum = output

            # Derivative of output (activated sum) with respect to the weight (simplified jacobian)
            J_hat_zw = np.outer(self.input, np.diagonal(J_zsum))    # J_hat_zw is in denominator format (same shape as weights, so this works)

            # Weight gradient used in updating the weights. Uses chain rule. (Slide 55)
            J_lw = J_lz * J_hat_zw

            # Adding regularization gradient to weight updates
            if self.regularization == "L1":
                J_lw += self.reg_rate * np.sign(self.weights)
            if self.regularization == "L2":
                J_lw += self.reg_rate * self.weights        # Adding the derivative of the L2 regularization w.r.t the weights

            # Derivative of output with respect to input (used in calculating loss w.r.t input for passing upstream)
            J_zy = np.dot(J_zsum, self.weights.T)       # Slide 50

            # Accumulating batch weight and bias gradients for updating later
            if self.batch_len == 0:  # First batch (reset nablaW and B)
                self.delta_w = J_lw
                self.delta_b = J_lz
            else:
                self.delta_w += J_lw
                self.delta_b += J_lz
            self.batch_len += 1

            if self.batch_len % self.batch_size == 0:
                self.gradient_descent(self.lr)

            # Loss gradient w.r.t input (sent upstream) (chain rule)
            # J_lz * J_zy
            J_ly = np.dot(J_lz, J_zy)       # Chain rule

            return J_ly
        else:
            return 0


    def gradient_descent(self, lr):
        self.delta_w = self.delta_w / self.batch_len      # Nudge weights and biases after batch is finished
        self.delta_b = self.delta_b / self.batch_len
        self.batch_len = 0

        self.weights -= lr * self.delta_w  # Adjusting weights  (-n* dE/dw)k
        self.biases -= lr * self.delta_b  # Adjusting bias


    """**** Activation functions and their derivatives (including softmax) ****"""

    # The derivative of all these must be implemented too:
    # Softmax is also a kind of activation function, represent as its own layer? Need to calculate the derivative of it
    def ReLU(self, Z):           # Not computationally expensive. Avoids vanishing gradient problem. Should only-
        return np.maximum(0, Z)  # be used within hidden layers of a NN. Gradients can be fragile during training.

    def ReLU_prime(self, Z):
        return np.where(Z > 0, 1.0, 0.0)
        # ALT: return (Z > 0).astype(Z.dtype)

    def sigmoid(self,Z):        # Tendency to saturate and kill gradients, not zero-centered, can lead to bad learning
        sig = 1/(1+np.exp(-Z))  # Usually only used for output layers and binary classifications
        return sig

    def sigmoid_prime(self,Z):
        sig = self.sigmoid(Z)*(1 - self.sigmoid(Z))   # f(z)(1 - f(z))
        return sig

    def tanh(self,Z):            # Usually used for hidden layers (values -1 to 1), helps center the data
        return np.tanh(Z)        # to make learning for the next layer easier

    def tanh_prime(self,Z):
        return 1.0 - np.tanh(Z)**2

    def linear(self,Z):
        return Z

    def linear_prime(self, Z):
        return 1

    def softmax(self, Z):
        return np.exp(Z) / np.sum(np.exp(Z))

    # Numerically stable sigmoid function
    def stable_sigmoid(self, x):
        if x >= 0:
            z = math.exp(-x)
            sig = 1 / (1 + z)
            return sig
        else:
            z = math.exp(x)
            sig = z / (1 + z)
            return sig

    def fetch_weights(self):
        return self.weights


    # Some helper functions for class

    def get_act_method(self):
        return self.act_method

    def get_n_nodes(self):
        return self.n_neurons
