import numpy as np
import random
import matplotlib.pyplot as plt
from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split

class dataGen:

    def __init__(self):
        self.dataset = []
        self.noise = 0.0    # Initializing amount of noise
        self.rand_pos = True

        self.train_pct = 0.7
        self.val_pct = 0.2
        self.test_pct = 0.1

        self.width = 0
        self.height = 0

        self.images = 100

        self.X_train = None
        self.y_train = None
        self.X_test = None
        self.y_test = None
        self.X_val = None
        self.y_val = None



    def generate_dataset(self,width, height,images, noise, train_pct, test_pct, val_pct, rand_pos):
        """
        n - image size
        images - amount of images
        noise - noise level (0-1)
        train_pct - percent of the dataset containing training samples
        val_pct  - percent of the dataset containing validation samples
        test_pct - percent of the dataset containing validation samples
        """
        self.width = width
        self.height = height
        self.noise = noise
        self.rand_pos = rand_pos
        self.train_pct = train_pct
        self.val_pct = val_pct
        self.test_pct = test_pct
        self.images = images

        samples = []
        labels = []
        count = 0
        for i in range(images):
            count += 1
            if count == 5:
                count = 1
            img = None
            #rndint = random.randint(1, 4)
            rndint = count
            if rndint == 1:
                img = self.make_circle(np.zeros((self.height, self.width)))
            if rndint == 2:
                img = self.make_cross(np.zeros((self.height, self.width)))
            if rndint == 3:
                img = self.make_rectangle(np.zeros((self.height, self.width)))
            if rndint == 4:
                img = self.make_dot(np.zeros((self.height, self.width)))

            samples.append(img)
            target = np.full(4, 0)
            target[rndint-1] = 1

            labels.append(target)

        self.X_train, self.y_train, self.X_test, self.y_test, self.X_val, self.y_val = self.divide_dataset(samples, labels)

        plt.imshow(self.X_train[0],cmap=plt.cm.gray)
        plt.show()


    def fetch_complete_data(self):
        return self.X_train, self.y_train, self.X_test, self.y_test, self.X_val, self.y_val

    def fetch_flattened_data(self):
        xtrain = []
        xtest = []
        xval = []

        for i in range(len(self.X_train)):
            xtrain.append(self.X_train[i].flatten().T)
        for i in range(len(self.X_test)):
            xtest.append(self.X_test[i].flatten().T)
        for i in range(len(self.X_val)):
            xval.append(self.X_val[i].flatten().T)

        return xtrain, self.y_train, xtest, self.y_test, xval, self.y_val


    def divide_dataset(self,X, y):
        sX, sy = shuffle(X, y, random_state=0)
        length = len(X)

        max = int(length*self.train_pct)
        X_train = sX[0:max]
        y_train = sy[0:max]
        min = max
        max = int(length*self.train_pct + length*self.test_pct)
        X_test = sX[min:max]
        y_test = sy[min:max]
        min = max
        max = int(length*self.train_pct + length*self.test_pct + length*self.val_pct)
        X_val = sX[min:max]
        y_val = sy[min:max]

        return X_train, y_train, X_test, y_test, X_val, y_val


    def make_circle(self,x):
        m, n = x.shape
        n = min(m, n)       # Selecting least length

        r = int(random.uniform(n/4, n/2))       # Picking radius
        offsetx = 0      # Random offset
        offsety = 0      # Random offset

        if r == int(n/2):   # Getting the space to the edges
            diff = 0
        else:
            diff = int(n/2) - r

        if self.rand_pos:
            offsetx = random.randint(-diff, diff)
            offsety = random.randint(-diff, diff)

        for i in range(n):
            for j in range(n):
                val = (i-int(n/2))**2 + (j-int(n/2))**2 - r**2
                val_inner = (i-int(n/2))**2 + (j-int(n/2))**2 - (r-1)**2

                if r > 3:
                    if float(val) < float(5):
                        if i+offsetx < n:
                            if j+offsety < n:
                                x[i+offsetx, j+offsety] = 1  # Rim of circle
                    if float(val_inner) < float(5):
                        x[i+offsetx, j+offsety] = 0     # inside of circle is set to 0
                else:
                    if float(val) < float(4):
                        x[i+offsetx-1, j+offsety-1] = 1

        return self.add_noise(x)

    def make_rectangle(self, x):
        m, n = x.shape
        n = np.minimum(m, n)

        while True:
            width = random.randint(3, self.width-2)
            height = random.randint(3, self.height-2)
            if width != height:     # Not a rectangle
                break

        if self.rand_pos:
            posx = random.randint(0, self.width - width - 1)
            posy = random.randint(0, self.height - height - 1)
        else:
            posx = int(self.width/2) - int(width/2)
            posy = int(self.height/2) - int(height/2)

        x[posx:posx + 1, posy:posy + height] = 1
        x[posx:posx + width, posy + height:posy + height + 1] = 1
        x[posx + width:posx + width + 1, posy:posy + height + 1] = 1
        x[posx:posx + width, posy:posy + 1] = 1

        return self.add_noise(x)

    def make_cross(self,x):
        m, n = x.shape
        n = np.minimum(m, n)
        width = 0
        height = 0
        if self.rand_pos:
            posx = int(n/2)
            posy = int(n/2)
        else:
            posx = random.randint(4,n-2)
            posy = random.randint(4,n-2)

        if n-posx > int(n/2):      # Distance to left wall is the smallest
            if 3>(n-posx):
                width = 3
            else:
                width = random.randint(3, posx)   # min 3 size : all the way to the left
        else:
            if 3>(n-posx):
                width = 3
            else:
                width = random.randint(3,n-posx)
        if n-posy > int(n/2):      # Distance to top is the smallest
            if 3>(n-posy):
                height = 3
            else:
                height = random.randint(3,posy)
        else:
            if 3>(n-posy):
                height = 3
            else:
                height = random.randint(3,n-posy)
        if width % 2 != 0 and width+1 < n:
            width += 1
        if height % 2 != 0 and height+1 < n:
            height += 1
        x[posx:posx+1, posy-int(height/2):posy+int(height/2)] = 1
        x[posx-int(width/2):posx+int(width/2), posy:posy+1] = 1

        return self.add_noise(x)

    def make_horizontal_lines(self, x):     # Model performs bad with this
        m, n = x.shape
        n = np.minimum(m, n)

        n_pos = random.randint(0,int(n/4))  # Amount of lines
        for i in range(n_pos):
            ypos = random.randint(0, self.height-1)
            x[ypos, 0:self.width] = 1

        return self.add_noise(x)

    def make_vertical_lines(self,x):     # Model performs bad with this
        m, n = x.shape
        n = np.minimum(m, n)

        n_pos = random.randint(0, int(n/4))  # Amount of lines
        for i in range(n_pos):
            ypos = random.randint(0, self.width-1)
            x[0:self.height, ypos] = 1

        return self.add_noise(x)

    def make_dot(self, x):
        m, n = x.shape
        n = np.minimum(m, n)

        x_pos = np.random.randint(0, int(m/2))
        y_pos = np.random.randint(0, int(n/2))
        diffx = x_pos - (m-1)
        diffy = y_pos - (n-1)
        x[y_pos:y_pos+diffy, x_pos:x_pos+diffx] = 1

        return self.add_noise(x)

    def add_noise(self,x):
        m, n = x.shape
        n = np.minimum(m, n)
        iters = int(n*self.noise)   # Adjusting the amount of noise
        for _ in range(iters):
            x[random.randint(0,n-1), random.randint(0,n-1)] = 1
            x[random.randint(0,n-1), random.randint(0,n-1)] = 0

        return x

    def show_images(self, images):
        imgs = images.copy()
        np.random.shuffle(imgs)
        for img in imgs[0:10]:
            plt.imshow(img, cmap=plt.cm.gray)
            plt.show()


