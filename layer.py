import numpy as np

# Abstract class delivering requiring implemented methods
class Layer:

    def __init__(self, n_inputs, n_neurons):
        self.input = None
        self.output = None


    def forward_pass(self, input):
        raise NotImplementedError


    def backward_pass(self, output_error, learning_rate):
        raise NotImplementedError
